include "session_configs.thrift"
include "generic_types.thrift"
include "broadcasts.thrift"
namespace cpp clooponline.packet
namespace csharp CloopOnline.Packets

struct CreateSession
{
    1: required session_configs.SessionConfigs configs
    2: optional i32            side
}

struct CreateSessionResponse
{
	1: required i32           sessionID,
	2: required i64           serverTime
}

struct JoinSession
{
	1: required i32                  sessionID,
	2: optional string               password,
	3: optional i32                  side
}

struct JoinSessionResponse
{
    1: required session_configs.SessionConfigs configs,
    2: required i64    serverTime,
    3: optional generic_types.DOMNode properties,
    4: optional list<broadcasts.RPCCalled> cachedRPCs
}

struct Joined
{
	1: required i32    sessionID,
	2: required i64    UID,
	3: required i32    side
}

struct Subscribe
{
	1: required session_configs.SessionConfigs  configs,
	2: required double          indicator,
	3: required i32             poolTime,
	4: optional double          maxIndicatorDistance,
	5: optional i32             side
}

struct SubscribeResponse
{
	1: required i64    trackID
}

struct SubscriptionResult
{
	1: required i64               trackID,
	2: required i32               sessionID,
	3: required JoinSessionResponse  joinObject
}

struct Unsubscribe
{
	1: required i64 trackID
}

struct ChangeSide
{
	1: required i32   sessionID,
	2: required i32   newSide,
	3: optional i64   asUID
}

struct SideChanged
{
	1: required i32    sessionID,
	2: required i64    UID,
	3: required i32    newSide
}

struct ChangeConfigs
{
	1: required i32            sessionID,
	2: required session_configs.SessionConfigs configs
}

struct ConfigsChanged
{
	1: required i32            sessionID,
	2: required session_configs.SessionConfigs configs,
}

struct LeaveSession
{
	1: required i32 sessionID
}

struct TemporaryLeave
{
	1: required i32 sessionID
}

struct TemporaryLeft
{
	1: required i32    sessionID,
	2: required i64    UID
}

struct SessionList
{
	1: required map<string, string>   filterOptions
}


struct SessionListResponse
{
	1: required map<i32, session_configs.SessionConfigs>  IDtoConfigs
}

struct BeManager
{
	1: required i32 sessionID,
}