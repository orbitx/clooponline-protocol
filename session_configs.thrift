include "generic_types.thrift"
namespace cpp clooponline.packet
namespace csharp CloopOnline.Packets

enum DisconnectionActionType
{
	KICK = 0,
	TERMINATE,
	CONTINUE_AND_KICK_ON_TIMEOUT,
	CONTINUE_AND_TERMINATE_ON_TIMEOUT,
	LOCK_AND_KICK_ON_TIMEOUT,
	LOCK_AND_TERMINATE_ON_TIMEOUT
}

struct SessionConfigs
{
	1: optional map<i32, i32>  sideToCapacity,
    2: optional map<i64, i32>  participantsToSide,
    3: optional list<i64>      tempLeftParticipants,
    4: optional i32            propertyRevisionIndex,
    5: optional i32            lastRPCIndex,
	6: optional bool           alwaysAcceptJoin,
	7: optional bool           canChangeSideAfterStart,
	8: optional DisconnectionActionType disconnectionBehavior,
	9: optional i32            disconnectionTimeout,
	10: optional i32           minPlayerToMatch,
	11: optional map<string, string> options,
	12: optional string        label,
	13: optional string        password,
	14: optional bool          isStarted
}