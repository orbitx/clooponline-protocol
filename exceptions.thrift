namespace cpp clooponline.packet
namespace csharp CloopOnline.Packets

enum ExceptionCode
{
	LOGIN_TIMEOUT = 1,
	NOT_IDENTIFIED,
	NOT_AUTHORIZED_APP,
	INVALID_PACKET,
	INVALID_SID,
	INVALID_TID,
	INVALID_AID,
	NOT_MANAGER,
	NOT_A_PARTICIPANT,
	NOT_DISCONNECTED,
	SIDE_NOT_AVAILABLE,
	INVALID_SESSION_STATE,
	ALREADY_STATED_READY,
	LOGIN_ERROR,
	WRONG_OTP,
	WRONG_OPTIONS,
	OPERATION_NOT_PERMITED,
	INVALID_RESTRICTIONS,
    UNEXPECTED_EXCEPTION,
}

const map<ExceptionCode, string> EXCEPTION_DESCRIPTION_MAP = {
	ExceptionCode.LOGIN_TIMEOUT: "login time has been expired",
	ExceptionCode.NOT_IDENTIFIED: "it is not possible to do this unless you identify yourself first",
	ExceptionCode.NOT_AUTHORIZED_APP: "this game server is not authorized to serve this application",
    ExceptionCode.INVALID_PACKET: "there was an error parsing the passed packet",
    ExceptionCode.INVALID_SID: "there is no open session with the specified SID",
    ExceptionCode.INVALID_TID: "there is no available subscription with the provided track ID",
    ExceptionCode.INVALID_AID: "the client's application ID, is not consistent with the given request",
    ExceptionCode.NOT_MANAGER: "this player is not the manager of the specified session hence does not have the privalage to do this action",
    ExceptionCode.NOT_A_PARTICIPANT: "this player or the specified one does not belong to the specified session",
    ExceptionCode.NOT_DISCONNECTED: "the specified player did not temporarely left the session, hence it is impossible to do this action",
    ExceptionCode.SIDE_NOT_AVAILABLE: "the requested side is not available for participation",
    ExceptionCode.INVALID_SESSION_STATE: "the state of the session does not comply with the action this player wants to do",
    ExceptionCode.ALREADY_STATED_READY: "this players already stated his readiness hence stating ready again, has no effect",
    ExceptionCode.LOGIN_ERROR: "there was an error when tried to inquiry the login state of this user",
    ExceptionCode.WRONG_OTP: "the provided OTP was wrong",
	ExceptionCode.WRONG_OPTIONS: "cannot match the provided options with the session",
	ExceptionCode.OPERATION_NOT_PERMITED: "you dont have permission to do this operation",
	ExceptionCode.INVALID_RESTRICTIONS: "the operation cannot be done with the provided restriction on it",
	ExceptionCode.UNEXPECTED_EXCEPTION: "unexpected error, see the description"
	}

struct Exception
{
	1: required ExceptionCode  errorCode,
	2: optional string         description
}