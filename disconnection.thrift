include "generic_types.thrift"
include "session_configs.thrift"
include "broadcasts.thrift"
include "subscribtion.thrift"
namespace cpp clooponline.packet 
namespace csharp CloopOnline.Packets

struct ManagerKick
{
    1: required i32    sessionID,
    2: required i64    UID,
}

enum LeavingReason
{
	ON_DEMAND = 1,
	KICK,
	DISCONNECTION
}

struct Left
{
	1: required i32    sessionID,
	2: required i64    UID,
	3: required LeavingReason reason
}

struct GetActiveSessions
{
}

struct ActiveSessions
{
	1: required map<i32, session_configs.SessionConfigs>  sessionIDToConfigs
}

enum ActiveSessionActionType
{
	JOIN = 0,
	LEAVE
}

struct HandleActiveSession
{
	1: required i32                     sessionID,
	2: required ActiveSessionActionType action,
	3: optional i32                     lastCachedRPCIndex,
	4: optional i32                     lastPropertiesRevisionIndex
}

struct HandleActiveSessionResponse
{
	1: required subscribtion.JoinSessionResponse  joinObject
}

struct Rejoined
{
	1: required i32    sessionID,
	2: required i64    UID,
}
