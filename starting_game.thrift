namespace cpp clooponline.packet
namespace csharp CloopOnline.Packets

struct Start
{
	1: required i32   sessionID,
	2: required i32   timeout,
	3: required bool  allShouldBeReadyFlag,
	4: required bool  playersCanCancelFlag
}

struct Ready
{
	1: required i32   sessionID,
	2: required i32   timeout
}

struct SetReady
{
	1: required i32   sessionID
}

struct CancelStart
{
	1: required i32    sessionID
}

enum StartCancelReason
{
	ON_DEMAND = 1,
	SOMEONE_LEFT,
	ALL_NOT_READY
}

struct StartCanceled
{
	1: required i32               sessionID,
	2: required StartCancelReason reason,
	3: required list<i64>         culprits
}

struct Started
{
	1: required i32                 sessionID,
	2: required map<i64, i32>       UIDToSideMap,
	3: required i64                 serverTime
}
