namespace cpp clooponline.packet
namespace csharp CloopOnline.Packets

struct Locked
{
	1: required i32    sessionID,
	2: required i64    UID
}

struct Resumed
{
	1: required i32    sessionID
}

struct Terminate
{
	1: required i32    sessionID,
}

enum TerminationReason
{
	ON_DEMAND = 1,
	DISCONNECTION
}

struct Terminated
{
	1: required i32    sessionID,
	2: required TerminationReason reason
}