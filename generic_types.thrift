namespace cpp clooponline.packet
namespace csharp CloopOnline.Packets

enum ValueType
{
    BOOL = 0,
    BYTE,
    INT_16,
    INT_32,
    INT_64,
    DOUBLE,
    STRING,
    MAP,
    ARRAY
}

struct DOMNode
{
	1: required ValueType  type;
    2: optional bool         boolVal;
    3: optional byte         byteValue;
    4: optional i16          int16Val;
    5: optional i32          int32Val;
    6: optional i64          int64Val;
    7: optional double       doubleVal;
    8: optional string       strVal;
    10: optional map<string, DOMNode> children;
    11: optional list<DOMNode>        array;
}