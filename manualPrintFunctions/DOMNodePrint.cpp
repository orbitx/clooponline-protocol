void DOMNode::printTo(std::ostream& out) const {
  switch (type)
  {
    case ValueType::BOOL:
      out << "bool(" << boolVal << ")";
        break;
    case ValueType::BYTE:
      out << "byte(" << byteValue << ")";
        break;
    case ValueType::INT_16:
      out << "int16(" << int16Val << ")";
        break;
    case ValueType::INT_32:
      out << "int32(" << int32Val << ")";
        break;
    case ValueType::INT_64:
      out << "int64(" << int64Val << ")";
        break;
    case ValueType::DOUBLE:
      out << "double(" << doubleVal << ")";
        break;
    case ValueType::STRING:
      out << "string(" << strVal << ")";
        break;
    case ValueType::MAP:
    {
      out << "{";
      bool first = true;
      for (auto& child : children)
      {
        if (first)
          first = false;
        else
          out << ", ";
        out << child.first << ":";
        child.second.printTo(out);
      }
      out << "}";
      break;
    }
    case ValueType::ARRAY:
    {
      out << "[";
      bool first = true;
      for (auto& ins : array)
      {
        if (first)
          first = false;
        else
          out << ", ";
        ins.printTo(out);
      }
      out << "]";
      break;
    }
  }
}
