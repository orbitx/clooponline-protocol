include "generic_types.thrift"

namespace cpp clooponline.packet
namespace csharp CloopOnline.Packets

struct RPCCall
{
	1: required i32                                sessionID,
	2: required string                             methodname,
	3: required map<byte, generic_types.DOMNode>   parameters,
	4: optional bool                               cached,
	5: optional list<i64>                          receptors,
	6: optional i64                                asUID
}

struct RPCCallResponse
{
	1: required i32    RPCindex,
	2: required i64    serverTime	
}

struct RPCCalled
{
	1: required i64    UID,
	2: required i32    sessionID,
	3: required i32    RPCindex,
	4: required string methodname,
	5: required map<byte, generic_types.DOMNode> parameters
	6: required i64    serverTime
}

enum OperationType
{
	MODIFY = 0,
	TRY_MODIFY,
	DELETE
}

struct PropertyInstance
{
	1: required string                 path,
	2: required OperationType          operation,
	3: optional generic_types.DOMNode  value,
	4: optional generic_types.DOMNode  cachedValue
}

struct SetProperties
{
	1: required i32                     sessionID,
	2: required list<PropertyInstance>  properties
}

enum SetPropertyResult
{
	OK = 0,
	WARNING_TYPE_CHANGED,
	ERROR_PATH_SYNTAX_ERROR,
	ERROR_INVALID_ARRAY_INDEX,
	ERROR_INDEX_OUT_OF_RANGE,
	ERROR_INVALID_CACHED_VALUE
}

struct SetPropertiesResponse
{
	1: required list<SetPropertyResult> resultFlags,
	2: optional i32                     revisionIndex,
	3: required i64                     serverTime
}

struct PropertiesChanged
{
	1: required i32                    sessionID,
	2: required i64                    UID,
	3: required list<PropertyInstance> changes,
	4: required i32                    revisionIndex,
	5: required i64                    serverTime
}

struct GetProperties
{
	1: required i32                     sessionID
}

struct GetPropertiesResponse
{
	1: required generic_types.DOMNode   properties
}
