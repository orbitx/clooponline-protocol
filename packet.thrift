include "identification.thrift"
include "subscribtion.thrift"
include "starting_game.thrift"
include "broadcasts.thrift"
include "disconnection.thrift"
include "pause.thrift"
include "exceptions.thrift"

namespace cpp clooponline.packet
namespace csharp CloopOnline.Packets

enum PacketType
{
	IDENTIFY = 0,

	CREATE_SESSION,
	CREATE_SESSION_RESPONSE,
	JOIN_SESSION,
	JOIN_SESSION_RESPONSE,
	JOINED,
	SUBSCRIBE,
	SUBSCRIBE_RESPONSE,
	SUBSCRIPTION_RESULT,
	UNSUBSCRIBE,
	CHANGE_SIDE,
	SIDE_CHANGED,
	CHANGE_CONFIGS,
	CONFIG_CHANGED,
	LEAVE_SESSION,
	TEMP_LEAVE,
	TEMP_LEFT,
	SESSION_LIST,
	SESSION_LIST_RESPONES,
	BE_MANAGER,

	START,
	READY,
	SET_READY,
	CANCEL_START,
	START_CANCELED,
	STARTED,

	RPC_CALL,
	RPC_CALL_RESPONSE,
	RPC_CALLED,
	SET_PROPERTIES,
	SET_PROPERTIES_RESPONSE,
	GET_PROPERTIES,
	GET_PROPERTIES_RESPONSE,
	PROPERTIES_CHANGED,

	MANAGER_KICK,
	LEFT,
	GET_AVAILABLE_ACTIVE_SESSIONS,
	AVAILABLE_ACTIVE_SESSIONS,
	HANDLE_ACTIVE_SESSION,
	HANDLE_ACTIVE_SESSION_RESPONSE,
	REJOINED,

	LOCKED,
	RESUMED,
	TERMINATE,
	TERMINATED,

	EXCEPTION
}

struct Packet
{
	1:  required PacketType                  msgType,
	2:  optional i64                         smagic,
	3:  optional i64                         cmagic,

	4:  optional identification.Identify   	              pIdentify,

	5:  optional subscribtion.CreateSession               pCreateSession,
	6:  optional subscribtion.CreateSessionResponse       pCreateSessionResponse,
	7:  optional subscribtion.JoinSession                 pJoinSession,
	8:  optional subscribtion.JoinSessionResponse         pJoinSessionResponse,
	9:  optional subscribtion.Joined                      pJoined,
	10: optional subscribtion.Subscribe                   pSubscribe,
	11: optional subscribtion.SubscribeResponse           pSubscribeResponse,
	12: optional subscribtion.SubscriptionResult          pSubscriptionResult,
	13: optional subscribtion.Unsubscribe                 pUnsubscribe,
	14: optional subscribtion.ChangeSide                  pChangeSide,
	15: optional subscribtion.SideChanged                 pSideChanged,
	16: optional subscribtion.ChangeConfigs               pChangeConfigs,
	17: optional subscribtion.ConfigsChanged              pConfigsChanged,
	18: optional subscribtion.LeaveSession                pLeaveSession,
	19: optional subscribtion.TemporaryLeave              pTemporaryLeave,
	20: optional subscribtion.TemporaryLeft               pTemporaryLeft,
	21: optional subscribtion.SessionList                 pSessionList,
	22: optional subscribtion.SessionListResponse         pSessionListResponse,
	23: optional subscribtion.BeManager                   pBeManager,

	24: optional starting_game.Start                       pStart,
	25: optional starting_game.Ready                       pReady,
	26: optional starting_game.SetReady                    pSetReady,
	27: optional starting_game.CancelStart                 pCancelStart,
	30: optional starting_game.StartCanceled               pStartCanceled,
	31: optional starting_game.Started                     pStarted,

	32: optional broadcasts.RPCCall                     pRPCCall,
	33: optional broadcasts.RPCCallResponse             pRPCCallResponse,
	34: optional broadcasts.RPCCalled                   pRPCCalled,
	35: optional broadcasts.SetProperties               pSetProperties,
	36: optional broadcasts.SetPropertiesResponse       pSetPropertiesResponse,
	37: optional broadcasts.GetProperties               pGetProperties,
	38: optional broadcasts.GetPropertiesResponse       pGetPropertiesResponse,
	39: optional broadcasts.PropertiesChanged           pPropertiesChanged,

	40: optional disconnection.ManagerKick                 pManagerKick,
	41: optional disconnection.Left                        pLeft,
	42: optional disconnection.ActiveSessions              pActiveSessions,
	43: optional disconnection.GetActiveSessions           pGetActiveSessions,
	44: optional disconnection.HandleActiveSession         pHandleActiveSession,
	45: optional disconnection.HandleActiveSessionResponse pHandleActiveSessionResponse,
	46: optional disconnection.Rejoined                    pRejoined,

	47: optional pause.Locked                      pLocked,
	48: optional pause.Resumed                     pResumed,
	49: optional pause.Terminate                   pTerminate,
	50: optional pause.Terminated                  pTerminated,

	51: optional exceptions.Exception              pException
}